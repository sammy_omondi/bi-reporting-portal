﻿'use strict';

var paths = {};

paths.css = 'Content/css/';
paths.toMin = ['Content/css/*.css', '!Content/css/*.min.css'];
paths.sass = 'Content/scss/**/*.scss';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps');



/**
 * Compile SASS to CSS
 */
gulp.task('sass', function () {
    return gulp.src('Content/scss/sci.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(paths.css));
});

/**
 * Watch SASS files
 */
gulp.task('sass:watch', function () {
    gulp.watch(paths.sass, gulp.series('sass', 'cssmin'));
});

/**
 * Minify css files
 */
gulp.task('cssmin', function () {
    return gulp.src(paths.toMin)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min', sourceMap: true }))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(paths.css));
});


/**
 * Build scripts
 */
gulp.task('build', gulp.series('sass', 'cssmin'));

gulp.task('default', gulp.series(gulp.parallel('build')));
