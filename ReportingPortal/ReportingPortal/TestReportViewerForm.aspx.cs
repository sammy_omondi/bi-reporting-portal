﻿using System;
using System.Security.Principal;
using System.Net;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


using Microsoft.Reporting.WebForms;

namespace Reporting_Portal
{
    public partial class TestReportViewerForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool canview;
            if(HttpContext.Current.User.IsInRole("98ea9f63-70ba-4a54-a5cb-dba7d7f1bd77"))
            {
                canview = true;
            }
        }

     
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ReportViewer1.ServerReport.ReportServerCredentials = new MyReportServerCredentials();
                ReportViewer1.LocalReport.Refresh();
                //// Set the processing mode for the ReportViewer to Remote  
                //ReportViewer1.ProcessingMode = ProcessingMode.Remote;

                //ServerReport serverReport = ReportViewer1.ServerReport;

                //// Set the report server URL and report path  
                //serverReport.ReportServerUrl =
                //    new Uri("https://<Server Name>/reportserver");
                //serverReport.ReportPath =
                //    "/AdventureWorks Sample Reports/Sales Order Detail";

                //// Create the sales order number report parameter  
                //ReportParameter salesOrderNumber = new ReportParameter();
                //salesOrderNumber.Name = "SalesOrderNumber";
                //salesOrderNumber.Values.Add("SO43661");

                //// Set the report parameters for the report  
                //reportViewer.ServerReport.SetParameters(
                //    new ReportParameter[] { salesOrderNumber });
            }
        }

    }
}
[Serializable]
public sealed class MyReportServerCredentials : IReportServerCredentials
{
    public WindowsIdentity ImpersonationUser
    {
        get
        {
            // Use the default Windows user.  Credentials will be
            // provided by the NetworkCredentials property.
            return null;
        }
    }


    public ICredentials NetworkCredentials
    {
        get
        {
            // Read the user information from the Web.config file.  
            // By reading the information on demand instead of 
            // storing it, the credentials will not be stored in 
            // session, reducing the vulnerable surface area to the
            // Web.config file, which can be secured with an ACL.

            // User name
            string userName =
                ConfigurationManager.AppSettings
                    ["MyReportViewerUser"];

            if (string.IsNullOrEmpty(userName))
                throw new Exception(
                    "Missing user name from web.config file");

            // Password
            string password =
                ConfigurationManager.AppSettings
                    ["MyReportViewerPassword"];

            if (string.IsNullOrEmpty(password))
                throw new Exception(
                    "Missing password from web.config file");

            // Domain
            string domain =
                ConfigurationManager.AppSettings
                    ["MyReportViewerDomain"];

            if (string.IsNullOrEmpty(domain))
                throw new Exception(
                    "Missing domain from web.config file");

            return new NetworkCredential(userName, password, domain);
        }
    }

    public bool GetFormsCredentials(out Cookie authCookie,
                out string userName, out string password,
                out string authority)
    {
        authCookie = null;
        userName = null;
        password = null;
        authority = null;

        // Not using form credentials
        return false;
    }
}