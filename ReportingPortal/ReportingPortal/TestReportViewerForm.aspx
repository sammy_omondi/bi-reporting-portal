﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestReportViewerForm.aspx.cs" Inherits="Reporting_Portal.TestReportViewerForm" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <dl>
            <dt>IsAuthenticated</dt>
            <dd><%= HttpContext.Current.User.Identity.IsAuthenticated %></dd>
<dt>AuthenticationType</dt>
<dd><%= HttpContext.Current.User.Identity.AuthenticationType %></dd>
<dt>Name</dt>
<dd><%= HttpContext.Current.User.Identity.Name %></dd>
<dt>Is in "group1"</dt>
<dd><%= HttpContext.Current.User.IsInRole("98ea9f63-70ba-4a54-a5cb-dba7d7f1bd77") %></dd>
<dt>Is in "group2"</dt>
<dd><%= HttpContext.Current.User.IsInRole("ac892106-2255-4259-99a8-f00baf358e4b") %></dd>
        </dl>

 <asp:ScriptManager runat="server"></asp:ScriptManager>        
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="100%" Height="700px">
            <ServerReport ReportServerUrl="http://scipbirstest.uksouth.cloudapp.azure.com/ReportServer" ReportPath="/AMS Reports/Portfolio Analysis" />
        </rsweb:ReportViewer>
    </form>
</body>
</html>
