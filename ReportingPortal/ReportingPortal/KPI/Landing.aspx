﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="Reporting_Portal.KPI.Landing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>IP KPIs</h3>
    <asp:Table ID="Table1" runat="server" CssClass="table">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>Report</asp:TableHeaderCell><asp:TableHeaderCell>Description</asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnDashboard1" runat="server" Text="KPI Dashboard 1" OnClick="lbtnDashboard1_Click" /></asp:TableCell><asp:TableCell>SCI Office KPIs</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnDashboard2" runat="server" Text="KPI Dashboard 2" OnClick="lbtnDashboard2_Click" /></asp:TableCell><asp:TableCell>KPIs by Member</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnDashboard3" runat="server" Text="KPI Dashboard 3" OnClick="lbtnDashboard3_Click" /></asp:TableCell><asp:TableCell>Region Overview​</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnDashboard4" runat="server" Text="KPI Dashboard 4" OnClick="lbtnDashboard4_Click" /></asp:TableCell><asp:TableCell>Member Overview</asp:TableCell></asp:TableRow>
        </asp:Table>
</asp:Content>
