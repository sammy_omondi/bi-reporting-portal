﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Reporting_Portal.KPI
{
    public partial class Landing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnDashboard4_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["KPIReport"] = "/IP KPIs/Dashboard4 - Member Overview";
            Session["ReportName"] = "Dashboard4 - Member Overview";
            Response.Redirect("KPIReports.aspx");
           
        }

        protected void lbtnDashboard3_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            //Session["KPIReport"] = "/IP KPIs/Copy (2) of Dashboard1 - SCI Office KPIs";
            Session["AMSReport"] = "/IP KPIs/Dashboard3 - Region Overview";
            Session["ReportName"] = "Dashboard3 - Region Overview";
            Response.Redirect("KPIReports.aspx");
        }

        protected void lbtnDashboard2_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["KPIReport"] = "/IP KPIs/Dashboard2 - KPIs by Member";
            Session["ReportName"] = "Dashboard2 - KPIs by Member";
            Response.Redirect("KPIReports.aspx");
           
        }

        protected void lbtnDashboard1_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["KPIReport"] = "/IP KPIs/Dashboard1 - SCI Office KPIs";
            Session["ReportName"] = "Dashboard1 - SCI Office KPIs";
            Response.Redirect("KPIReports.aspx");
        }
    }
}