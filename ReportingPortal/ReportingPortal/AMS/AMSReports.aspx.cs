﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;

namespace Reporting_Portal.AMS
{
    public partial class AMSReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Slows Down Page Loading, commented out for now
            //SiteMap.SiteMapResolve += new SiteMapResolveEventHandler(this.ExpandForumPaths);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ReportViewer1.ServerReport.ReportServerCredentials = new MyReportServerCredentials();
                ReportViewer1.LocalReport.Refresh();

                // Set the processing mode for the ReportViewer to Remote  
                ReportViewer1.ProcessingMode = ProcessingMode.Remote;

                ServerReport serverReport = ReportViewer1.ServerReport;

                string ssrsUrl = ConfigurationManager.AppSettings["SSRSServer"];

                // Set the report server URL and report path  
                serverReport.ReportServerUrl = new Uri(ssrsUrl);
                serverReport.ReportPath = Session["AMSReport"].ToString();

                //// Create the sales order number report parameter  
                //ReportParameter salesOrderNumber = new ReportParameter();
                //salesOrderNumber.Name = "SalesOrderNumber";
                //salesOrderNumber.Values.Add("SO43661");

                //// Set the report parameters for the report  
                //reportViewer.ServerReport.SetParameters(
                //    new ReportParameter[] { salesOrderNumber });
            }
        }

        private SiteMapNode ExpandForumPaths(Object sender, SiteMapResolveEventArgs e)
        {
            // The current node represents a Post page in a bulletin board forum. 
            // Clone the current node and all of its relevant parents. This 
            // returns a site map node that a developer can then 
            // walk, modifying each node.Url property in turn. 
            // Since the cloned nodes are separate from the underlying 
            // site navigation structure, the fixups that are made do not 
            // effect the overall site navigation structure.
            SiteMapNode currentNode = SiteMap.CurrentNode.Clone(true);
            SiteMapNode tempNode = currentNode;

            currentNode.Title = Session["ReportName"].ToString();

            return currentNode;
        }

    }
}