﻿<%@ Page Title="AMS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="Reporting_Portal.AMS.Landing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>AMS Reports</h3>
    <asp:Table ID="Table1" runat="server" CssClass="table">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>Report</asp:TableHeaderCell><asp:TableHeaderCell>Description</asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnUserSummary" runat="server" Text="User Summary" OnClick="lbtnUserSummary_Click" /></asp:TableCell><asp:TableCell>Provides by AMS User a list of live awards in which he/she is listed on various capacity: - Point Person - Proposal Lead, Contributer/Reviewer, Project Manager, Technical Advisor, - Responsible for Report and claim</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnUserAdmin" runat="server" Text="User Admin Report" OnClick="lbtnUserAdmin_Click" /></asp:TableCell><asp:TableCell>Provides a snapshot by Country / Members, of numbers and details of AMS users by type of Permission (Approver, Editor, Reader, Super User)</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnAwardInfoSheet" runat="server" Text="Award Info Sheet" OnClick="lbtnAwardInfoSheet_Click" /></asp:TableCell><asp:TableCell>Provides by Award an up to date information included in AMS. It is an almost full copy of the Award record that can be extracted in Excel, Word or PDF and will be very useful to use in kick-off meetings and to send to offices with low connectivity.</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnPartnershipAnalysis" runat="server" Text="Partnership Analysis" OnClick="lbtnPartnershipAnalysis_Click" /></asp:TableCell><asp:TableCell>Provide for a selected portfolio of awards, Numbers and Amounts in chosen currency of Current Active and Pipeline and Pre-closeout Partner Agreements; with a detailed list of related Partner Agreements</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnPortfolioAnalysis" runat="server" Text="Portfolio Analysis" OnClick="lbtnPortfolioAnalysis_Click" /></asp:TableCell><asp:TableCell>Provide for a selected portfolio of awards, Numbers and Amounts in chosen currency of Award at various stages (Pipeline, PALs, Active etc...) , with a detailed list of related awards</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnAwardReportingSchedule" runat="server" Text="Award Reporting Schedule" OnClick="lbtnAwardReportingSchedule_Click" /></asp:TableCell><asp:TableCell>Provides for a selected portfolio of awards at a certain date: - A list of reports due (to Member and Donors) by categories on Active and Pre-close out awards. - Past reporting performance of reports due and reports submitted each months</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnMatchMarketPlace" runat="server" Text="Match Market Place" OnClick="lbtnMatchMarketPlace_Click" /></asp:TableCell><asp:TableCell>This report looks into all awards that have a match requirement from the donor. This report displays the total match required, value of secured match and the match gap.</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnMonthlyTeamWorkPlan" runat="server" Text="Monthly Team Workplan" OnClick="lbtnMonthlyTeamWorkPlan_Click" /></asp:TableCell><asp:TableCell>Provides for a selected portfolio of awards /Team, a list of tasks due within 30 days (including backlog or not): - Proposals due - Reports due - Awards Awaiting Activation / Ending / Pre or pending close out - Donor payment claims or payments due</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnPartnerREportingSchedule" runat="server" Text="Partner Reporting Schedule" OnClick="lbtnPartnerREportingSchedule_Click" /></asp:TableCell><asp:TableCell>Analysis of reports to partners, such as due, overdue. Past reporting performance of reports due and reports received each month</asp:TableCell></asp:TableRow>
    </asp:Table>
</asp:Content>
