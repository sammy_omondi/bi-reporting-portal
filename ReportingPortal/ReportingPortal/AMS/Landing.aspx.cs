﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Reporting_Portal.AMS
{
    public partial class Landing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnUserSummary_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/User Summary";
            Session["ReportName"] = "User Summary";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnUserAdmin_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/User Admin Report";
            Session["ReportName"] = "User Admin Report";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnAwardInfoSheet_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Award Info Sheet";
            Session["ReportName"] = "Award Info Sheet";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnPartnershipAnalysis_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Partnership Analysis";
            Session["ReportName"] = "Partnership Analysis";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnPartnerREportingSchedule_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Partner Reporting Schedule";
            Session["ReportName"] = "Partner Reporting Schedule"; 
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnMonthlyTeamWorkPlan_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Monthly Team Workplan";
            Session["ReportName"] = "Monthly Team Workplan";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnMatchMarketPlace_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Match Market Place";
            Session["ReportName"] = "Match Market Place";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnAwardReportingSchedule_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Award Reporting Schedule";
            Session["ReportName"] = "Award Reporting Schedule";
            Response.Redirect("AMSReports.aspx");
        }

        protected void lbtnPortfolioAnalysis_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/AMS/Portfolio Analysis";
            Session["ReportName"] = "Portfolio Analysis";
            Response.Redirect("AMSReports.aspx");
        }
    }
}