﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Reporting_Portal.HART
{
    public partial class Landing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnLiveOMT_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["HARTReport"] = "/HART/Live OMT";
            Session["ReportName"] = "Live OMT";
            Response.Redirect("HARTReports.aspx");
        }

        protected void lbtnCompletedOMT_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["HARTReport"] = "/HART/Completed OMT";
            Session["ReportName"] = "Live OMT";
            Response.Redirect("HARTReports.aspx");
        }

        protected void lbtnResponsesAwaitingApproval_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["HARTReport"] = "/HART/Approvals/OMT Approvals";
            Session["ReportName"] = "Live OMT";
            Response.Redirect("HARTReports.aspx");
        }
    }
}