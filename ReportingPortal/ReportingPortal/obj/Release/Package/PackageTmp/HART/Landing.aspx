﻿<%@ Page Title="HART" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="Reporting_Portal.HART.Landing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>HART Reports</h3>
    <asp:Table ID="Table1" runat="server" CssClass="table">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>Report</asp:TableHeaderCell><asp:TableHeaderCell>Description</asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnLiveOMT" runat="server" Text="Live OMT" OnClick="lbtnLiveOMT_Click" /></asp:TableCell><asp:TableCell>Live Humanitarian Response Reports</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnCompletedOMT" runat="server" Text="Completed OMT" OnClick="lbtnCompletedOMT_Click" /></asp:TableCell><asp:TableCell>Completed Humanitarian Response Reports</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnResponsesAwaitingApproval" runat="server" Text="Responses Awaiting Approval " OnClick="lbtnResponsesAwaitingApproval_Click" /></asp:TableCell><asp:TableCell>Humanitarian Response Reports Awaiting Approval</asp:TableCell></asp:TableRow>
        </asp:Table>
</asp:Content>
