﻿<%@ Page Title="AMS Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AMSReports.aspx.cs" Inherits="Reporting_Portal.AMS.AMSReports" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <div>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="100%" Height="1000px">
            <ServerReport ReportServerUrl="http://scipbirsprod.uksouth.cloudapp.azure.com/ReportServer" ReportPath="/AMS/Portfolio Analysis" />
            </rsweb:ReportViewer>
        </div>
</asp:Content>
