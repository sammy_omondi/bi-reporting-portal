﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Reporting_Portal.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>SCI Business Intelligence Team</h3>
    <address>
        St Vincent House<br />
        30 Orange Street<br />
        WC2H 7HH<br />
        UK<br />
        <abbr title="Phone">P:</abbr>
        +44 (0) 20 3272 0300 
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:analytics@savethechildren.org">analytics@savethechildren.org</a><br />
    </address>
</asp:Content>
