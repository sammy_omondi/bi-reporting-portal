﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace Reporting_Portal.BIAdmin
{
    public partial class BIAdminReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ReportViewer1.ServerReport.ReportServerCredentials = new MyReportServerCredentials();
                ReportViewer1.LocalReport.Refresh();

                // Set the processing mode for the ReportViewer to Remote  
                ReportViewer1.ProcessingMode = ProcessingMode.Remote;

                ServerReport serverReport = ReportViewer1.ServerReport;

                string ssrsUrl = ConfigurationManager.AppSettings["SSRSServer"];

                // Set the report server URL and report path  
                serverReport.ReportServerUrl = new Uri(ssrsUrl);
                serverReport.ReportPath = Session["AMSReport"].ToString();

               
            }
        }
    }
}