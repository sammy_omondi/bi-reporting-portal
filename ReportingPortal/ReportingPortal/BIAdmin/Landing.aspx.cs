﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Reporting_Portal.BIAdmin
{
    public partial class Landing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnProduction_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/BI Admin/Production/ETLDetails";
            Session["ReportName"] = "ETL Details";
            Response.Redirect("BIAdminReports.aspx");
        }

        protected void lbtnProuctionLegacy_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/BI Admin/Production Legacy/ETLDetails";
            Session["ReportName"] = "ETL Details";
            Response.Redirect("BIAdminReports.aspx");
        }

        protected void lbtnTest_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/BI Admin/Test/ETLDetails";
            Session["ReportName"] = "ETL Details";
            Response.Redirect("BIAdminReports.aspx");
        }

        protected void lbtnTestLegacy_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/BI Admin/Test Legacy/ETLDetails";
            Session["ReportName"] = "ETL Details";
            Response.Redirect("BIAdminReports.aspx");
        }

        protected void lbtnPrepProd_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/BI Admin/PREPROD/ETLDetails";
            Session["ReportName"] = "ETL Details";
            Response.Redirect("BIAdminReports.aspx");
        }

        protected void lbtnDevelopment_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["AMSReport"] = "/BI Admin/Development/ETLDetails";
            Session["ReportName"] = "ETL Details";
            Response.Redirect("BIAdminReports.aspx");
        }
    }
}