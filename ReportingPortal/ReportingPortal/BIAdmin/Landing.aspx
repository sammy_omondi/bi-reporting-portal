﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="Reporting_Portal.BIAdmin.Landing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>BI Admin</h3>
    <asp:Table ID="Table1" runat="server" CssClass="table">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>Report</asp:TableHeaderCell><asp:TableHeaderCell>Description</asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnProduction" runat="server" Text="Production" OnClick="lbtnProduction_Click" /></asp:TableCell><asp:TableCell>​AZU-BI-PROD-01 ETL Daily Details</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnProuctionLegacy" runat="server" Text="Production Legacy" OnClick="lbtnProuctionLegacy_Click" /></asp:TableCell><asp:TableCell>​GB3ST4-BI01 ETL Daily Details</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnTest" runat="server" Text="Test" OnClick="lbtnTest_Click" /></asp:TableCell><asp:TableCell>AZU-BI-TEST-01 ETL Daily Details</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnTestLegacy" runat="server" Text="Test Legacy" OnClick="lbtnTestLegacy_Click" /></asp:TableCell><asp:TableCell>GB3ST4-BITEST01 ETL Daily Details​</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnPrepProd" runat="server" Text="PREPROD" OnClick="lbtnPrepProd_Click" /></asp:TableCell><asp:TableCell>AZU-BI-PREPROD-01 ETL Daily Details</asp:TableCell></asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:LinkButton ID="lbtnDevelopment" runat="server" Text="Development" OnClick="lbtnDevelopment_Click" /></asp:TableCell><asp:TableCell>AZU-BI-DEV-01 ETL Daily Details</asp:TableCell></asp:TableRow>
        </asp:Table>
</asp:Content>