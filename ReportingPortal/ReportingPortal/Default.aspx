﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Reporting_Portal._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Save the Children Reporting</h1>
        <p class="lead">Welcome to the Save the Children Reporting Portal</p>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p>
                The reporting portal uses advanced reporting framework to combine data from many IT Systems and present it in an easy to analysis interface that allows people to make data driven decisions.
            </p>
            <p>
                Some of the applications provided include:
            </p>
            <p>AMS Reports</p>
            <p>IP KPI Reports</p>
            <p>TIM Reports</p>
            <p>HART Reports</p>
            <p>HRIS Reports</p>
        </div>
    </div>

</asp:Content>
